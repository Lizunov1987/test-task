﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using test.DataAccess;

namespace test.Controllers
{
    public class FounderController : Controller
    {
        readonly ApplicationContext db;
        public FounderController(ApplicationContext context)
        {
            db = context;
        }

        public IActionResult Founders(int clientId)
        {
            IEnumerable<Founder> founders = db.Founders.Where(c => c.ClientId == clientId).ToList();
            ViewBag.ClientId = clientId;
            return View(founders);
        }

        public IActionResult AllFounders()
        {
            IEnumerable<Founder> founders = db.Founders;
            return View("Founders", founders);
        }

        public IActionResult AddFounder(int clientId)
        {
            return View(clientId);
        }

        [HttpPost]
        public IActionResult AddFounder(Founder founder)
        {
            founder.CreateDate = DateTime.Now;
            founder.UpdateDate = DateTime.Now;
            db.Founders.Add(founder);
            db.SaveChanges();
            return RedirectToAction("Founders", founder.ClientId);
        }

        public IActionResult EditFounder(int founderId)
        {
            var founder = db.Founders.Find(founderId);
            return View(founder);
        }

        [HttpPost]
        public IActionResult EditFounder(Founder founder)
        {
            founder.UpdateDate = DateTime.Now;
            db.Update(founder);
            db.SaveChanges();
            return RedirectToAction("Founders");
        }

        [HttpPost]
        public IActionResult DeleteFounder(int founderId)
        {
            var founder = db.Founders.Find(founderId);
            if (founder != null)
            {
                db.Founders.Remove(founder);
                db.SaveChanges();
            }
            return RedirectToAction("Founders");
        }
    }
}