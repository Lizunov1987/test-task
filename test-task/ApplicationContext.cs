﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace test_task
{
    class ApplicationContext:DbContext
    {
        public ApplicationContext():base("Data Source=.\\SQLEXPRESS;Initial Catalog=Demo;Integrated Security=True;")
        {
        }

        public DbSet<Client> Clients { get; set; }
        public DbSet<Founder> Founders { get; set; }
    }
}
