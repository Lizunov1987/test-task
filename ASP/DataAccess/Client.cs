﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace test.DataAccess
{
    public class Client
    {
        [Key]
        public int ClientId { get; set; }

        [RegularExpression(@"^[а-яА-Я]{1,50}$", ErrorMessage ="Неверное имя")]
        //[Required(ErrorMessage = "Не указано имя")]
        //[MaxLength(50, ErrorMessage ="Длина имени не должна превышать 50 букв")]
        public string Name { get; set; }

        [RegularExpression(@"^[\d]{10,12}$", ErrorMessage ="Неверно введен ИНН")]
        //[Required(ErrorMessage = "Не указан ИНН")]
        //[StringLength(10, MinimumLength =10, ErrorMessage ="Неверная длина ИНН")]
        public string INN { get; set; }
        public bool IsIndividual { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }

        public List<Founder> Founders { get; set; }
    }
}
