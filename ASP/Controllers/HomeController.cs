﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using test.DataAccess;

namespace test.Controllers
{

    public class HomeController : Controller
    {
        readonly ApplicationContext db;
        public HomeController(ApplicationContext context)
        {
            db = context;
        }

        public IActionResult Index()
        {
            return View(db.Clients);
        }

        [HttpGet]
        public IActionResult AddClient()
        {
            return View();
        }

        [HttpPost]
        public IActionResult AddClient(Client client)
        {
            if (!ModelState.IsValid)
                return Content($"{client.Name} - {client.INN}");

            client.CreateDate = client.UpdateDate = DateTime.Now;
            db.Clients.Add(client);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult EditClient(int clientId)
        {
            return View(db.Clients.Find(clientId));
        }

        [HttpPost]
        public IActionResult EditClient(Client client)
        {
            client.UpdateDate = DateTime.Now;
            db.Update(client);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult DeleteClient(int id)
        {
            var client = db.Clients.Find(id);
            if (client != null)
            {
                db.Clients.Remove(client);
                db.SaveChanges();
            }
            return RedirectToAction("Index");
        }
    }
}
