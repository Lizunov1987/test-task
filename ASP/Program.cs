using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;



namespace test
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        //https://docs.microsoft.com/ru-ru/aspnet/core/fundamentals/host/generic-host?view=aspnetcore-3.0
        //In versions of ASP.NET Core earlier than 3.0, the Web Host is used for HTTP workloads. 
        //The Web Host is no longer recommended for web apps and remains available only for backward compatibility.
        public static IHostBuilder CreateHostBuilder(string[] args) =>  
            Host.CreateDefaultBuilder(args)                         
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
