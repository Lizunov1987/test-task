﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test_task
{
    class Client
    {
        [Key]
        public int ClientId { get; set; }
        [MaxLength(30)]
        public string Name { get; set; }
        [MaxLength(16)]
        public string INN { get; set; } 
        public bool IsIndividual { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }

        public ICollection<Founder> Founders;
    }
}
