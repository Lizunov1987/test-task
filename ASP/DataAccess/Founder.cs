﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace test.DataAccess
{
    public class Founder
    {
        [Key]
        public int FounderId { get; set; }

        [Required(ErrorMessage = "Не указано имя")]
        [MaxLength(30)]
        public string Name { get; set; }

        [Required(ErrorMessage = "Не указана фамилия")]
        [MaxLength(30)]
        public string Surname { get; set; }
        [MaxLength(30)]        
        public string Patronymic { get; set; }

        [Required(ErrorMessage = "Не указан ИНН")]
        [StringLength(12, MinimumLength = 10, ErrorMessage = "Неверная длина ИНН")]
        public string INN { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime UpdateDate { get; set; }

        [Column("client")]
        public int ClientId { get; set; }
        public Client Client { get; set; }
    }
}
